var Url = '/api/clientes/';
var contenido = document.querySelector('#users-table');


function getClientes() {
    return fetch(Url)
            .then(Response => Response.json())
            .then(datos => {
                table(datos);
            });
}

function table(datos) {
    contenido.innerHTML = '';
    for (let valor of datos) {
        contenido.innerHTML += `  
                <tr>
                    <th scope="row">${valor.id_cliente}</th>
                    <td>${valor.nombre}</td>
                    <td>${valor.apellido}</td>
                    <td>${valor.correo}</td>
                    <td>${valor.direccion}</td>
                    <td>${valor.telefono}</td>
                    <td>
                        <a onclick="window.location='update_clientes.html'"><button type="button" class="btn btn-warning" onclick='guardarDato(${valor.id_cliente})'>Editar</button></a>
                        <a onclick="window.location='clientes.html'"><button type="button" class="btn btn-danger" onclick='deleteCliente(${valor.id_cliente})'>Eliminar</button></a>                           
                    </td>
                </tr>         
        `;
    }
}

function GetCrearCliente() {
    const datos = fetch(Url, {
        method: 'POST',
        body: JSON.stringify({
            nombre: document.getElementById("nombre").value,
            apellido: document.getElementById("apellido").value,
            correo: document.getElementById("correo").value,
            direccion: document.getElementById("direccion").value,
            telefono: document.getElementById("telefono").value
        }),
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(res => res.json())
            .then(datos => {
                console.log(datos);
            });

}

function deleteCliente(idCliente) {
    fetch('/api/clientes/' + idCliente, {
        method: 'DELETE'
    })
            .then(res => res.text())
            .then(res => console.log(res));
}


function UpdateCliente() {
    fetch(Url, {
        method: 'PUT',
        body: JSON.stringify({
            id_cliente: document.getElementById("id_cliente").value,
            nombre: document.getElementById("nombre").value,
            apellido: document.getElementById("apellido").value,
            correo: document.getElementById("correo").value,
            direccion: document.getElementById("direccion").value,
            telefono: document.getElementById("telefono").value
        }),
        headers: {
            "Content-type": "application/json"
        }

    })
            .then(res => res.json())
            .then(datos => {
                console.log(datos);
            });
}

var form = document.querySelector('#formUpdate');

function guardarDato(id) {    
    localStorage.setItem('idcliente', id);
}

var id = localStorage.getItem('idcliente');

function rellenarCampos() {
    fetch(Url + id)
            .then(Response => Response.json())
            .then(datos => {

                form.innerHTML = '';
                form.innerHTML += `  
                    <div class="mb-3">
                        <label for="product-name" class="form-label">id_cliente</label>
                        <input type="number" class="form-control" id="id_cliente" name="id_cliente" value="${datos.id_cliente}">
                    </div>
                    <div class="mb-3">
                        <label for="product-name" class="form-label">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="${datos.nombre}">
                    </div>
                    <div class="mb-3">
                        <label for="product-name" class="form-label">Apellido</label>
                        <input type="text" class="form-control" id="apellido" name="apellido" value="${datos.apellido}">
                    </div>
                    <div class="mb-3">
                        <label for="product-name" class="form-label">Correo</label>
                        <input type="text" class="form-control" id="correo" name="correo" value="${datos.correo}">
                    </div>
                    <div class="mb-3">
                        <label for="product-quantity" class="form-label">Direccion</label>
                        <input type="text" class="form-control" id="direccion" name="direccion" value="${datos.direccion}">
                    </div>
                    <div class="mb-3">
                        <label for="product-quantity" class="form-label">Telefono</label>
                        <input type="number" class="form-control" id="telefono" name="telefono" value="${datos.telefono}">
                    </div>
                    <a onclick="window.location = 'clientes.html'"><button type="button" class="btn btn-primary" onclick="UpdateCliente()">Guardar</button></a>                     
                `;
            
            });

}
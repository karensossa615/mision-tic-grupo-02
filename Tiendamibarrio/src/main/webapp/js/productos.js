var Url = '/api/productos/';
var contenido = document.querySelector('#products-table');

function getProductos() {
    return fetch(Url)
            .then(Response => Response.json())
            .then(datos => {
                table(datos);
            });
}

function table(datos) {

    contenido.innerHTML = '';
    for (let valor of datos) {
        contenido.innerHTML += `  
                <tr>
                    <th scope="row">${valor.id_producto}</th>
                    <td>${valor.nombre_producto}</td>
                    <td>${valor.precio}</td>
                    <td>${valor.categoria}</td>
                    <td>${valor.cantidad}</td>
                    <td>${valor.imagen}</td>
                    <td>
                        <a onclick="window.location='Update_productos.html'"><button type="button" class="btn btn-warning" onclick='guardarDato(${valor.id_producto})'>Editar</button></a>
                        <a onclick="window.location='productos.html'"><button type="button" class="btn btn-danger" onclick='deleteProducto(${valor.id_producto})'>Eliminar</button></a>                           
                    </td>
                </tr>         
        `;
    }
}

//Metodo q envia los cambios en el producto a la base de datos en methos PUT
function UpdateProducto() {    
    fetch(Url, {
        method: 'PUT',
        body: JSON.stringify({
            id_producto: document.getElementById("id_producto").value,
            nombre_producto: document.getElementById("nombre_producto").value,
            precio: document.getElementById("precio").value,
            categoria: document.getElementById("categoria").value,
            cantidad: document.getElementById("cantidad").value,
            imagen: document.getElementById("imagen").value
        }),
        headers: {
            "Content-type": "application/json"
        },

    })
            .then(res => res.json())
            .then(datos => {
                console.log(datos);
            });
}


function deleteProducto(idProducto) {
    fetch('/api/productos/' + idProducto, {
        method: 'DELETE'
    })
            .then(res => res.text())
            .then(res => console.log(res));
}


function GetCrearProductos() {
    const datos = fetch(Url, {
        method: 'POST',
        body: JSON.stringify({
            nombre_producto: document.getElementById("nombre_producto").value,
            precio: document.getElementById("precio").value,
            categoria: document.getElementById("categoria").value,
            cantidad: document.getElementById("cantidad").value,
            imagen: document.getElementById("imagen").value
        }),
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(res => res.json())
            .then(datos => {
                console.log(datos);
            });

}

/*esta seccion del codigo hace la busqueda de los produtos para rellenar campos*/


var form = document.querySelector('#formUpdate');

function guardarDato(id) {    
    localStorage.setItem('idproductos', id);
}

var id = localStorage.getItem('idproductos');

function rellenarCampos(){
    fetch(Url+id)
            .then(Response => Response.json())
            .then(datos => {
                form.innerHTML = ``;
                form.innerHTML += `
                    <div class="mb-3">
                        <label for="product-name" class="form-label">id_Producto</label>
                        <input type="number" class="form-control" id="id_producto" name="id_producto" value="${datos.id_producto}">
                    </div>
                    <div class="mb-3">
                        <label for="product-name" class="form-label">Nombre Producto</label>
                        <input type="text" class="form-control" id="nombre_producto" name="nombre_producto" value="${datos.nombre_producto}">
                    </div>
                    <div class="mb-3">
                        <label for="product-sale-value" class="form-label">Precio</label>
                        <input type="number" class="form-control" id="precio" name="precio" value="${datos.precio}">
                    </div>
                    <div class="mb-3">
                        <label for="product-purchase-value" class="form-label">Categoria</label>
                        <input type="text" class="form-control" id="categoria" name="categoria" value="${datos.categoria}">
                    </div>
                    <div class="mb-3">
                        <label for="product-quantity" class="form-label">Cantidad_productos</label>
                        <input type="number" class="form-control" id="cantidad" name="cantidad" value="${datos.cantidad}">
                    </div>
                    <div class="mb-3">
                        <label for="product-quantity" class="form-label">Url Imagen</label>
                        <input type="text" class="form-control" id="imagen" name="imagen" value="${datos.imagen}">
                    </div>
                    <a onclick="window.location = 'productos.html'"><button type="button" class="btn btn-primary" onclick="UpdateProducto()">Guardar</button></a>
                `;
            });

}
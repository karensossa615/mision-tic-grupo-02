var Url = '/api/productos/';
var contenido = document.querySelector('#card');

function getCard() {
    fetch(Url)
            .then(Response => Response.json())
            .then(datos => {
                card(datos);
            });
}

function card(datos) {
    console.log(datos);
    contenido.innerHTML = ''
    for (let valor of datos) {
        contenido.innerHTML += ` 
        <div>
            <div class="card-header text-center">
                <label>${valor.nombre_producto}</label>
            </div>
            <div class="card-body">
                <i>$${valor.precio}</i>
                <img  src="${valor.imagen}" width="200" height="180">
            </div>
            <div class="card-footer text-center">            
                <div>
                    <a class="btn btn-outline-info" href="#">Agregar a carrito</a>                    
                </div> 
            </div> 
            <br>
        </div>
        `
    }
}
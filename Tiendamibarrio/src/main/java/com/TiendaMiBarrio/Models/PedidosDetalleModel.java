
package com.TiendaMiBarrio.Models;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name = "detallepedido")
public class PedidosDetalleModel implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle")
    private int id_detalle;

    @ManyToOne
    @JoinColumn(name = "id_producto")
    private ProductosModel id_producto;
    
    @ManyToOne
    @JoinColumn(name = "id_pedido")
    private PedidosModel id_pedido;
    
    @Column(name = "cantidad_compra")
    private int cantidad_compra;
    
    @Column(name = "total_producto")
    private int total_producto;

    public PedidosDetalleModel() {
    }

    public PedidosDetalleModel(int id_detalle, int cantidad_compra, int total_producto) {
        this.id_detalle = id_detalle;
        this.cantidad_compra = cantidad_compra;
        this.total_producto = total_producto;
    }

    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public ProductosModel getId_producto() {
        return id_producto;
    }

    public void setId_producto(ProductosModel id_producto) {
        this.id_producto = id_producto;
    }

    public PedidosModel getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(PedidosModel id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getCantidad() {
        return cantidad_compra;
    }

    public void setCantidad(int cantidad_compra) {
        this.cantidad_compra = cantidad_compra;
    }

    public int getTotal() {
        return total_producto;
    }

    public void setTotal(int total_producto) {
        this.total_producto = total_producto;
    }

    @Override
    public String toString() {
        return "PedidosDetalleModel{" + "id_detalle=" + id_detalle + ", id_producto=" + id_producto + ", id_pedido=" + id_pedido + ", cantidad_compra=" + cantidad_compra + ", total_producto=" + total_producto + '}';
    }   
    
}

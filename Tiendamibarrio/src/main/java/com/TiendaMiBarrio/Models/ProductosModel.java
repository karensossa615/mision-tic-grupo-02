package com.TiendaMiBarrio.Models;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "productos")
public class ProductosModel implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id_producto")
    private int id_producto;

    @Column(name = "nombre_producto")
    private String nombre_producto;

    @Column(name = "precio")
    private float precio;

    @Column(name = "categoria")
    private String categoria;

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "imagen")
    private String imagen;

    public ProductosModel() {
    }

    public ProductosModel(int id_producto, String nombre_producto, float precio, String categoria, int cantidad, String imagen) {
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.precio = precio;
        this.categoria = categoria;
        this.cantidad = cantidad;
        this.imagen = imagen;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "ProductosModel{" + "id_producto=" + id_producto + ", nombre_producto=" + nombre_producto + ", precio=" + precio + ", categoria=" + categoria + ", cantidad=" + cantidad + ", imagen=" + imagen + '}';
    }

}

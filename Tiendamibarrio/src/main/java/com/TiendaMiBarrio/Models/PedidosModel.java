package com.TiendaMiBarrio.Models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "pedidos")
public class PedidosModel implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido")
    private int id_pedido;   
    
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private ClientesModel id_cliente;

    @Column(name = "estado")
    private String estado;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "valor_total")
    private float valor_total;

    public PedidosModel() {
    }

    public PedidosModel(int id_pedido, String estado, Date fecha, float valor_total) {
        this.id_pedido = id_pedido;
        this.estado = estado;
        this.fecha = fecha;
        this.valor_total = valor_total;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }  

    public ClientesModel getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(ClientesModel id_cliente) {
        this.id_cliente = id_cliente;
    }    

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getValor_total() {
        return valor_total;
    }

    public void setValor_total(float valor_total) {
        this.valor_total = valor_total;
    }

    @Override
    public String toString() {
        return "PedidosModel{" + "id_pedido=" + id_pedido + ", id_cliente=" + id_cliente + ", estado=" + estado + ", fecha=" + fecha + ", valor_total=" + valor_total + '}';
    }    

}

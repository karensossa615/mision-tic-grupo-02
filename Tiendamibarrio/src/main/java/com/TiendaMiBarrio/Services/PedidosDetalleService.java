package com.TiendaMiBarrio.Services;

import com.TiendaMiBarrio.Models.PedidosDetalleModel;
import java.util.List;

public interface PedidosDetalleService {
    public PedidosDetalleModel save(PedidosDetalleModel producto);
    public void delete(Integer id);
    public PedidosDetalleModel findById(Integer id);
    public List<PedidosDetalleModel> findAll();
}

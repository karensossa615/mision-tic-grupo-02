
package com.TiendaMiBarrio.Services;

import com.TiendaMiBarrio.Models.ClientesModel;
import java.util.List;

public interface ClientesService { 
    public ClientesModel save(ClientesModel clientes);
    public void delete(Integer id);
    public ClientesModel findById(Integer id);
    public List<ClientesModel> findAll();  
}

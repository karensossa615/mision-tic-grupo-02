
package com.TiendaMiBarrio.Services;

import com.TiendaMiBarrio.Models.PedidosModel;
import java.util.List;

public interface PedidosService { 
        public PedidosModel save(PedidosModel pedidos);
        public void delete(Integer id);
        public PedidosModel findById(Integer id);
        public List<PedidosModel> findAll();   
}

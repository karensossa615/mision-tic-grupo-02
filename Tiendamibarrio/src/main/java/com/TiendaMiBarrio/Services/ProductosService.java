package com.TiendaMiBarrio.Services;

import com.TiendaMiBarrio.Models.ProductosModel;
import java.util.List;

public interface ProductosService {
    public ProductosModel save(ProductosModel producto);
    public void delete(Integer id);
    public ProductosModel findById(Integer id);
    public List<ProductosModel> findAll();
}

package com.TiendaMiBarrio.Services.Implement;

import com.TiendaMiBarrio.Dao.ProductosDao;
import com.TiendaMiBarrio.Models.ProductosModel;
import com.TiendaMiBarrio.Services.ProductosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductosServiceImpl implements ProductosService {

    @Autowired
    private ProductosDao productosDao;

    @Override
    @Transactional(readOnly = false)
    public ProductosModel save(ProductosModel productos) {
        return productosDao.save(productos);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        productosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ProductosModel findById(Integer id) {
        return productosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductosModel> findAll() {
        return (List<ProductosModel>) productosDao.findAll();
    }
}

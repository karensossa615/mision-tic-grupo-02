package com.TiendaMiBarrio.Services.Implement;

import com.TiendaMiBarrio.Dao.ClientesDao;
import com.TiendaMiBarrio.Models.ClientesModel;
import com.TiendaMiBarrio.Services.ClientesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientesServiceImpl implements ClientesService {

    @Autowired
    private ClientesDao clientesDao;

    @Override
    @Transactional(readOnly = false)
    public ClientesModel save(ClientesModel clientes) {
        return clientesDao.save(clientes);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        clientesDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ClientesModel findById(Integer id) {
        return clientesDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClientesModel> findAll() {
        return (List<ClientesModel>) clientesDao.findAll();
    }
}

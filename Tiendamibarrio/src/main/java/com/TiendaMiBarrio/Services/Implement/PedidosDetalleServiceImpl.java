package com.TiendaMiBarrio.Services.Implement;

import com.TiendaMiBarrio.Dao.PedidosDetalleDao;
import com.TiendaMiBarrio.Models.PedidosDetalleModel;
import com.TiendaMiBarrio.Services.PedidosDetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PedidosDetalleServiceImpl implements PedidosDetalleService {

    @Autowired
    private PedidosDetalleDao PedidosDetalledao;

    @Override
    @Transactional(readOnly = false)
    public PedidosDetalleModel save(PedidosDetalleModel pedidosdetalle) {
        return PedidosDetalledao.save(pedidosdetalle);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        PedidosDetalledao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PedidosDetalleModel findById(Integer id) {
        return PedidosDetalledao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PedidosDetalleModel> findAll() {
        return (List<PedidosDetalleModel>) PedidosDetalledao.findAll();
    }
}

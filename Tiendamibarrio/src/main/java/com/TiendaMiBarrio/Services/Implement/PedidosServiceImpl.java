package com.TiendaMiBarrio.Services.Implement;

import com.TiendaMiBarrio.Dao.PedidosDao;
import com.TiendaMiBarrio.Models.PedidosModel;
import com.TiendaMiBarrio.Services.PedidosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PedidosServiceImpl implements PedidosService {

    @Autowired
    private PedidosDao pedidosDao;

    @Override
    @Transactional(readOnly = false)
    public PedidosModel save(PedidosModel pedidos) {
        return pedidosDao.save(pedidos);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        pedidosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PedidosModel findById(Integer id) {
        return pedidosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PedidosModel> findAll() {
        return (List<PedidosModel>) pedidosDao.findAll();
    }
}

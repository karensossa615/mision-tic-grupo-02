
package com.TiendaMiBarrio.Dao;

import com.TiendaMiBarrio.Models.PedidosDetalleModel;
import org.springframework.data.repository.CrudRepository;


public interface PedidosDetalleDao extends CrudRepository<PedidosDetalleModel,Integer>  {
 
}

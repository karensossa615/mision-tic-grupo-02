
package com.TiendaMiBarrio.Dao;

import com.TiendaMiBarrio.Models.ClientesModel;
import org.springframework.data.repository.CrudRepository;


public interface ClientesDao  extends CrudRepository<ClientesModel,Integer>  {
    
}

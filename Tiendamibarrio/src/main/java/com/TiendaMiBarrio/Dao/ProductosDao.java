
package com.TiendaMiBarrio.Dao;

import com.TiendaMiBarrio.Models.ProductosModel;
import org.springframework.data.repository.CrudRepository;


public interface ProductosDao extends CrudRepository<ProductosModel,Integer> {
    
}

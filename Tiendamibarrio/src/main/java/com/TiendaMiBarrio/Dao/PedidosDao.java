
package com.TiendaMiBarrio.Dao;

import com.TiendaMiBarrio.Models.PedidosModel;
import org.springframework.data.repository.CrudRepository;


public interface PedidosDao extends CrudRepository<PedidosModel,Integer>{
    
}

package com.TiendaMiBarrio.Controllers;

import com.TiendaMiBarrio.Models.ClientesModel;
import com.TiendaMiBarrio.Services.ClientesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/clientes")
public class ClientesControllers {

    @Autowired
    private ClientesService clientesservice;
    
    @PostMapping(value = "/")
    public ResponseEntity<ClientesModel> agregar(@RequestBody ClientesModel clientesmodel) {
        ClientesModel obj = clientesservice.save(clientesmodel);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }   

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ClientesModel> eliminar(@PathVariable Integer id) {
        ClientesModel obj = clientesservice.findById(id);
        if (obj != null) {
            clientesservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<ClientesModel> editar(@RequestBody ClientesModel clientesmodel) {
        ClientesModel obj = clientesservice.findById(clientesmodel.getId_cliente());
        if (obj != null) {
            obj.setNombre(clientesmodel.getNombre());
            obj.setApellido(clientesmodel.getApellido());
            obj.setCorreo(clientesmodel.getCorreo());
            obj.setDireccion(clientesmodel.getDireccion());
            obj.setTelefono(clientesmodel.getTelefono());
            clientesservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/")
    public List<ClientesModel> consultarTodo() {
        return clientesservice.findAll();
    }

    @GetMapping("/{id}")
    public ClientesModel consultaPorId(@PathVariable Integer id) {
        return clientesservice.findById(id);
    }
}

package com.TiendaMiBarrio.Controllers;

import com.TiendaMiBarrio.Models.PedidosModel;
import com.TiendaMiBarrio.Services.PedidosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/pedidos")
public class PedidosControllers {  

        @Autowired
        private PedidosService pedidosservice;
        
        @PostMapping(value = "/")
        public ResponseEntity<PedidosModel> agregar(@RequestBody PedidosModel pedidosmodel) {
            PedidosModel obj = pedidosservice.save(pedidosmodel);
            return new ResponseEntity<>(obj, HttpStatus.OK);
        }

        @DeleteMapping(value = "/{id}")
        public ResponseEntity<PedidosModel> eliminar(@PathVariable Integer id) {
            PedidosModel obj = pedidosservice.findById(id);
            if (obj != null) {
                pedidosservice.delete(id);
            } else {
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(obj, HttpStatus.OK);
        }

        @PutMapping(value = "/")
        public ResponseEntity<PedidosModel> editar(@RequestBody PedidosModel pedidosmodel) {
            PedidosModel obj = pedidosservice.findById(pedidosmodel.getId_pedido());
            if (obj != null) {
                obj.setEstado(pedidosmodel.getEstado());
                obj.setFecha(pedidosmodel.getFecha());
                obj.setValor_total(pedidosmodel.getValor_total());
                pedidosservice.save(obj);
            } else {
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(obj, HttpStatus.OK);
        }

        @GetMapping("/")
        public List<PedidosModel> consultarTodo() {
            return pedidosservice.findAll();
        }

        @GetMapping("/{id}")
        public PedidosModel consultaPorId(@PathVariable Integer id) {
            return pedidosservice.findById(id);
        }
}

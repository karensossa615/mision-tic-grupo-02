package com.TiendaMiBarrio.Controllers;

import com.TiendaMiBarrio.Models.PedidosDetalleModel;
import com.TiendaMiBarrio.Services.PedidosDetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/detalle")
public class PedidosDetalleControllers {

    @Autowired
    private PedidosDetalleService pedidosdetalleservice;

    @PostMapping(value = "/")
    public ResponseEntity<PedidosDetalleModel> agregar(@RequestBody PedidosDetalleModel pedidosdetallemodel) {
        PedidosDetalleModel obj = pedidosdetalleservice.save(pedidosdetallemodel);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PedidosDetalleModel> eliminar(@PathVariable Integer id) {
        PedidosDetalleModel obj = pedidosdetalleservice.findById(id);
        if (obj != null) {
            pedidosdetalleservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<PedidosDetalleModel> editar(@RequestBody PedidosDetalleModel pedidosdetallemodel) {
        PedidosDetalleModel obj = pedidosdetalleservice.findById(pedidosdetallemodel.getId_detalle());
        if (obj != null) {
            obj.setCantidad(pedidosdetallemodel.getCantidad());
            obj.setTotal(pedidosdetallemodel.getTotal());
            pedidosdetalleservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/")
    public List<PedidosDetalleModel> consultarTodo() {
        return pedidosdetalleservice.findAll();
    }

    @GetMapping("/{id}")
    public PedidosDetalleModel consultaPorId(@PathVariable Integer id) {
        return pedidosdetalleservice.findById(id);
    }
}

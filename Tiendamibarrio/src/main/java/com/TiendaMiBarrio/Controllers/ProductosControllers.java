package com.TiendaMiBarrio.Controllers;

import com.TiendaMiBarrio.Models.ProductosModel;
import com.TiendaMiBarrio.Services.ProductosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/productos")
public class ProductosControllers {

    @Autowired
    private ProductosService productosservice;    
    
    @PostMapping(value = "/")
    public ResponseEntity<ProductosModel> agregar(@RequestBody ProductosModel productosmodel) {
        ProductosModel obj = productosservice.save(productosmodel);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }  
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ProductosModel> eliminar(@PathVariable Integer id) {
        ProductosModel obj = productosservice.findById(id);
        if (obj != null) {
            productosservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<ProductosModel> editar(@RequestBody ProductosModel productosmodel) {
        ProductosModel obj = productosservice.findById(productosmodel.getId_producto());
        if (obj != null) {
            obj.setNombre_producto(productosmodel.getNombre_producto());
            obj.setPrecio(productosmodel.getPrecio());
            obj.setCategoria(productosmodel.getCategoria());
            obj.setCantidad(productosmodel.getCantidad());
            obj.setImagen(productosmodel.getImagen());
            productosservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/")
    public List<ProductosModel> consultarTodo() {
        return productosservice.findAll();
    }

    @GetMapping("/{id}")
    public ProductosModel consultaPorId(@PathVariable Integer id) {
        return productosservice.findById(id);
    }
}

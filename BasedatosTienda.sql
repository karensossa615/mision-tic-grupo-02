-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tiendamibarrio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tiendamibarrio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendamibarrio` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `tiendamibarrio` ;

-- -----------------------------------------------------
-- Table `tiendamibarrio`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendamibarrio`.`clientes` (
  `id_cliente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `telefono` double NOT NULL,
  PRIMARY KEY (`id_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendamibarrio`.`pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendamibarrio`.`pedidos` (
  `id_pedido` INT NOT NULL AUTO_INCREMENT,
  `id_cliente` INT NOT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `fecha` DATE NOT NULL,
  `valor_total` FLOAT NOT NULL,
  PRIMARY KEY (`id_pedido`),
  INDEX `fk_id_cliente_idx` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_id_cliente`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `tiendamibarrio`.`clientes` (`id_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendamibarrio`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendamibarrio`.`productos` (
  `id_producto` INT NOT NULL AUTO_INCREMENT,
  `nombre_producto` VARCHAR(45) NOT NULL,
  `precio` FLOAT NULL DEFAULT NULL,
  `categoria` VARCHAR(45) NOT NULL,
  `cantidad` INT NOT NULL,
  `imagen` VARCHAR(999) NULL DEFAULT NULL,
  PRIMARY KEY (`id_producto`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tiendamibarrio`.`detallepedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendamibarrio`.`detallepedido` (
  `id_detalle_pedido` INT NOT NULL AUTO_INCREMENT,
  `id_producto` INT NOT NULL,
  `id_pedido` INT NOT NULL,
  `cantidad_compra` INT NOT NULL,
  `total_producto` INT NOT NULL,
  PRIMARY KEY (`id_detalle_pedido`),
  INDEX `fk_id_producto_idx` (`id_producto` ASC) VISIBLE,
  INDEX `fk_id_pedido_idx` (`id_pedido` ASC) VISIBLE,
  CONSTRAINT `fk_id_pedido`
    FOREIGN KEY (`id_pedido`)
    REFERENCES `tiendamibarrio`.`pedidos` (`id_pedido`),
  CONSTRAINT `fk_id_producto`
    FOREIGN KEY (`id_producto`)
    REFERENCES `tiendamibarrio`.`productos` (`id_producto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

# Mision TIC  2022 Grupo 02-O7

![UIS](MisionTIC-UIS.png)

# Integrantes
1. Deiby Pinilla:Desarrollador Backend 
2. Karen Tapias: Desarrollador Backend
3. Nidia Velandia:Desarrollador Backend 
4. Luly Garcia:Desarrollador Fronted
5. Christian Jimenez:Desarrollador Backend


# Componenentes
1. **Backend**: Este repositorio
2. **Frontend**: Este repositorio

# Proyecto Seleccionado

**Tienda Virtual (mibarrio.com)**: El ministerio TIC con el objetivo de llevar la transformación digital a los barrios ha decidido contratar a su empresa para que realice la digitalización de la operación de las tiendas de la ciudad, donde se pueda registrar los ingresos, egresos y el control de inventario de la mercancía.

### Mundo del problema

Hoy en día múltiples empresas pequeñas no cuentan con un sistema de registro digital que les facilite llevar el control de inventario de la tienda, ni les contribuya a realizar las ventas online. En la actualidad, las grandes y medianas empresas cuentan con sistemas que les permiten desarrollar este trabajo más fácilmente y tienen sus tiendas en línea. Existen diversas aplicaciones y páginas en el mercado que contribuyen a este objetivo, pero que algunas veces quedan fuera del alcance de los pequeños comercios por sus precios; entre estas aplicaciones se encuentran por ejemplo: Mywebsite Ecommerce, Magento, Oxid, Chopify, entre otras.

Es por eso que el principal objetivo de nuestro proyecto es ayudar a gestionar los ingresos y egresos, el control de inventario de la mercancía y facilite un acceso a ventas en línea  de microempresas, como es el caso de una tienda de barrio.


### Objetivos

1.Diseñar una aplicación que ayude a gestionar los inventarios de mercancía de una microempresa.
2.Diseñar una aplicación que actualice, consulte, inserte y elimine los productos de un inventario de mercancías
3.Crear una aplicación que ayude a la optimización del tiempo del tendero y del cliente en sus respectivas compras.


### Requerimientos funcionales

---

<table>
  <tr>
    <th>Nombre</th>
    <td>Crear  productos en la base de datos del inventario</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
    Se busca adicionar productos nuevos en la base de datos del inventario de manera que pueda ser usada su información con posterioridad.
    </p>
    </td>
  </tr>
  <tr>
</table>

---

<table>
  <tr>
    <th>Nombre</th>
    <td>Consultar el inventario existente</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
    La aplicación permitirá que la base de datos del inventario sea consultada por parte de los usuarios.
    </p>
    </td>
  </tr>
  <tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Consultar productos en el carro de compra </td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Media</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
   La aplicación permitirá  la consulta  de los productos existentes  del carro de compras de los usuarios.
    </p>
    </td>
  </tr>
  <tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Consultar los pedidos del cliente en el carrito a través de un botón de Pedido</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Media</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
    La aplicación permitirá la consulta de los pedidos del carro de compras a través de un botón de  pedidos.
    </p>
    </td>
  </tr>
  <tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Actualización del inventario</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
    La aplicación le permitirá al tendero actualizar e insertar productos el inventario
    </p>
    </td>
  </tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Actualización de datos de los productos</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
   La aplicación tendrá la opción de actualizar los datos del inventario.
    </p>
    </td>
  </tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Eliminar productos en el inventario</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
   La aplicación tendrá la opción de eliminar los productos del inventario.
    </p>
    </td>
  </tr>
</table>

----

<table>
  <tr>
    <th>Nombre</th>
    <td>Creación de una base de datos.</td>
  </tr>
  <tr>
    <th>Prioridad</th>
    <td>Alta</td>
  </tr>
  <tr>
    <th>Descripción</th>
    <td>
        <p>
   La aplicación creará una base de datos para el inventario.
    </p>
    </td>
  </tr>
</table>


### Requerimientos no funcionales

1. Seguridad y privacidad de datos
2. Diseño responsive
3. Manuales de usuario 

### Modelo de datos planteado

![Base de datos](Base.png)

### Gestión del marco de trabajo Scrum en Jira

##### Epicas

![Epicas](epics.png)


##### Sprints

![Sprints](spr.png)

##### Historias de usuario

![Historias de usuario sprints 1 ](spr1.png)

![Historias de usuario sprints 2 ](spr2.png)

![Historias de usuario sprints 3 ](spr3.png)

![Historias de usuario sprints 4 ](spr4.png)


### GitLab
**Ver proyecto** [Aquí](https://gitlab.com/karensossa615/mision-tic-grupo-02.git).

### Servidor
[Servidor](https://minticloud.uis.edu.co/c3s7grupo2/).

![Pagina](Pro.jpeg)

Logramos terminar el proyecto, sin embargo haremos algunos ajustes para perfeccionarlo.












